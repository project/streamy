<?php

/**
 * @file
 * Contains streamy.module..
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function streamy_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the streamy module.
    case 'help.page.streamy':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('My Awesome Module') . '</p>';
      return $output;
      break;
    default:
      break;
  }
}

/**
 * Used for the private file system.
 *
 * Implements hook_file_download().
 */
function streamy_file_download($uri) {
  $schemes = Drupal::service('streamy.factory')->getSchemes();
  $scheme = Drupal::service('file_system')->uriScheme($uri);

  if (!$scheme || !in_array($scheme, $schemes, TRUE)) {
    return;
  }

  return [
    'Content-Type'   => Drupal::service('file.mime_type.guesser.extension')
                              ->guess($uri),
    'Content-Length' => filesize($uri),
  ];
}

/**
 * Adds a custom validator for any form that allows to configure a Storage option.
 * (E.g. /admin/config/media/file-system or /admin/structure/types/manage/{contenttype}/fields/{fieldname}/storage)
 *
 * Implements hook_form_alter().
 */
function streamy_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  $settingsToAlter = [
    'field_storage_config_edit_form' => ['#validate' => '__streamy_field_storage_config_edit_form_validate'],
    'system_file_system_settings'    => ['#validate' => '__streamy_system_file_system_settings_validate'],
  ];
  if (array_key_exists($form_id, $settingsToAlter)) {
    $form['#validate'] = array_merge($form['#validate'], $settingsToAlter[$form_id]);
  }
}

/**
 * Custom hook validate for field_storage_config_edit_form.
 */
function __streamy_field_storage_config_edit_form_validate(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $currentScheme = $form_state->getValue(['settings', 'uri_scheme']);
  $form_element = &$form['settings']['uri_scheme'];
  if (is_array($form_element)) {
    __streamy_validate_filesystem_settings_form($currentScheme, $form_element, $form, $form_state);
  }
}

/**
 * Custom hook validate for system_file_system_settings.
 */
function __streamy_system_file_system_settings_validate(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $currentScheme = $form_state->getValue('file_default_scheme');
  $form_element = &$form['file_default_scheme'];
  if (is_array($form_element)) {
    __streamy_validate_filesystem_settings_form($currentScheme, $form_element, $form, $form_state);
  }
}

/**
 * Ensures that the selected streamy plugin is ready to be used, otherwise sets a form error on the $form_element.
 *
 * @param                                      $currentScheme
 * @param array                                $form_element
 * @param                                      $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 */
function __streamy_validate_filesystem_settings_form($currentScheme, array &$form_element, &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $streamyFactory = \Drupal::service('streamy.factory');
  $streamySchemes = $streamyFactory->getSchemes();
  if (in_array($currentScheme, $streamySchemes)) {
    $streamy = $streamyFactory->getFilesystem($currentScheme);
    if (!$streamy->ensure()) {
      $form_state->setError($form_element,
                            t('The selected stream <strong>%scheme</strong> is not properly configure and cannot be used in the current settings. '
                              . 'Please visit the Streamy settings page to configure it.',
                              ['%scheme' => $currentScheme]));
    }
  }
}
